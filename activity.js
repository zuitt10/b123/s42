let showFullNameDisplay = document.querySelector('#full-name-display');
let inputFirstName = document.querySelector('#txt-first-name');
let inputLastName = document.querySelector('#txt-last-name');
let submitBtn = document.querySelector('#submit-btn')

const showName = () => {
	console.log(inputFirstName.value)
	console.log(inputLastName.value)
	showFullNameDisplay.innerHTML = `${inputFirstName.value} ${inputLastName.value}`
}

inputFirstName.addEventListener('keyup',showName);

// you can actually create multpile event listener that run the same function.
inputLastName.addEventListener('keyup',showName);



submitBtn.addEventListener('click',()=>{

if(inputFirstName.value === "" || inputLastName.value === ""){
	alert('Please input firstname and  lastname')
} else{
	alert(`Thank you for registering ${inputFirstName.value} ${inputLastName.value}`)

}

})